new Vue({
    el: '#app',
    data: {
        users:[],
        currentLocal:{},
        rus:{
            name:'Имя',
            lastname:'Фамилия',
            phone:'Телефон'
        },
        eng:{
            name:'Name',
            lastname:'Lastname',
            phone:'Phone'
        },
        currentId: "ID"    
    },
    methods:{
        setLocalRus: function(){
            this.currentLocal = this.rus         
        },
        setLocalEng: function(){
            this.currentLocal = this.eng
        },
        deleteUser: function(index){
            this.users.splice(index, 1)
        },
        isUserExist: function(){
            for (var i = 0; i < this.users.length; i++) {
                if (this.users[i].id == this.currentId) {
                    return true
                }
            }
            return false
        },
        addUser: function(){
            elem = document.getElementById('idInput')
            if (isFinite(this.currentId) && this.currentId != ''){
                elem.classList.remove('has-error')
                if (!this.isUserExist()){
                    var params = this.currentId
                    this.$http.post('/getUser', params).then(function (response) {
                            if (!response.body){
                                alert('Такого пользователя не существует!!!')
                            }else{
                                this.users.push(response.body);
                                this.currentId = "ID";
                            }
                        },
                        function (error) {
                            console.log('error with getting data\n' + error)
                        })
                }
            }
            else{
                elem.classList.add('has-error')
            }
        }
    },
    created: function(){
        this.currentLocal = this.rus
    }
        /*function(){
        this.$http.post('/allUsers').then(function(response){
            this.users = response.body;
        },
        function(error){
            console.log('error with getting data\n' + error);
        }
        )
    }*/

})