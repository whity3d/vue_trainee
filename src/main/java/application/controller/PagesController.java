package application.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

@RestController
public class PagesController {

    private Map<String, String> pages = new HashMap<>();

    @PostConstruct
    private void init() throws IOException {
        File[] files = new File("src/main/resources/pages").listFiles();

        for (File file : files) {
            String fileName = file.getName().split("\\.")[0];
            Path filePath = file.toPath();

            pages.put(fileName, new String(Files.readAllBytes(filePath)));
        }
    }

    @GetMapping("/{page:^[^.]*$}")
    public String loginPage(@PathVariable String page) {
        return pages.get(page);
    }

}