package application.controller;

import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@RestController
public class SimpleController {

    public static class User {

        private Long id;
        private String nickname;
        private String phone;

        public User() {
        }

        public User(Long id, String nickname, String phone) {
            this.id = id;
            this.nickname = nickname;
            this.phone = phone;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }

    private List<User> users = new ArrayList<>();

    @PostConstruct
    private void init() {
        users.add(new User(1L, "rostishik", "+0000000001"));
        users.add(new User(2L, "Whity3D", "+0000000001"));
        users.add(new User(3L, "arcfire1", "+0000000002"));
        users.add(new User(4L, "aqwrel", "+0000000003"));
        users.add(new User(5L, "Veteran4eg", "+0000000004"));
    }

    @RequestMapping("/text")
    public String getText() {
        return "Text bla bla";
    }

    @PostMapping("/allUsers")
    public List<User> getUsers() {
        return users;
    }

    @PostMapping("/getUser")
    public User getUserById_paramExample(@RequestParam Long userId) {
        return users.stream()
                .filter(user -> user.id.equals(userId))
                .findFirst().orElse(null);
    }

    @PostMapping("/getUser/{userId}")
    public User getUserById_pathExample(@PathVariable Long userId) {
        return users.stream()
                .filter(user -> user.id.equals(userId))
                .findFirst().orElse(null);
    }

}
